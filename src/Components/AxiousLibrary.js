import { Component } from "react";
import axios from "axios";

class AxiosLibrary extends Component{
    axiosLibraryCallAPI = async(config) =>{
        let response = await axios(config);
        

        return response.data
    }

    getByIdHandler =() =>{
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getAllHandler =() =>{
        let congfig = {
            method : 'get',
            maxBodyLength : Infinity,
            url : 'https://jsonplaceholder.typicode.com/posts',
            header : {}
        }

        this.axiosLibraryCallAPI(congfig)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    createHandler =() =>{
        let data =JSON.stringify({
            "userId" : 1,
            "title" : "foo",
            "body" : "bar"
        });

        let config = {
            method : 'post',
            maxBodyLength : Infinity,
            url : 'https://jsonplaceholder.typicode.com/posts',
            header : {},
            data : data

        }

        this.axiosLibraryCallAPI(config)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    updateHandler =() =>{
        let data =JSON.stringify({
            "userId" : 1,
            "title" : "foo",
            "body" : "bar"
        });

        let config = {
            method : 'put',
            maxBodyLength : Infinity,
            url : 'https://jsonplaceholder.typicode.com/posts/1',
            header : {},
            data : data

        }

        this.axiosLibraryCallAPI(config)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    deleteHandler =() =>{
        let config = {
            method : 'delete',
            maxBodyLength : Infinity,
            url : 'https://jsonplaceholder.typicode.com/posts/1',
            header : {}
        }

        this.axiosLibraryCallAPI(config)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    render(){
        return(
            <>
                <div className="container">
                    <div className="row mt-5">
                        <div className="col">
                            <button onClick={this.getByIdHandler} className="btn btn-danger">GET BY ID</button>
                        </div>
                        <div className="col">
                            <button onClick={this.getAllHandler} className="btn btn-danger">GET ALL</button>
                        </div>
                        <div className="col">
                            <button onClick={this.createHandler} className="btn btn-danger">POST CREATE</button>
                        </div>
                        <div className="col">
                            <button onClick={this.updateHandler} className="btn btn-danger">PUT UPDATE</button>
                        </div>
                        <div className="col">
                            <button onClick={this.deleteHandler} className="btn btn-danger">DELETE</button>
                        </div>
                    </div>
                </div>
            </>
        )
    }

}

export default AxiosLibrary